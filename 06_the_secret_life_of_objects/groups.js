class Group {
    constructor() {
        this.set = []
    }

    add(item) {
        if(!this.has(item)) this.set.push(item);
    }

    delete(item) {
        this.set = this.set.filter(x => x !== item);
    }

    has(item) {
        return this.set.some(x => x === item);
    }

    static from(arr) {
        let group = new Group()
        arr.forEach(element => {
            group.add(element)
        });
        return group
    }
}



let group = Group.from([10, 20]);
console.log(group.has(10));
// → true
console.log(group.has(30));
// → false
group.add(10);
group.delete(10);
console.log(group.has(10));
// → false