class Group {
    constructor() {
        this.set = []
    }

    add(item) {
        if(!this.has(item)) this.set.push(item);
    }

    delete(item) {
        this.set = this.set.filter(x => x !== item);
    }

    has(item) {
        return this.set.some(x => x === item);
    }

    static from(arr) {
        let group = new Group()
        arr.forEach(element => {
            group.add(element)
        });
        return group
    }

    [Symbol.iterator]() {
        return new GroupIterator(this);
    }
}

class GroupIterator {
    constructor(group) {
        this.arr = group.set;
        this.idx = 0;
    }

    next() {
        if(this.idx == this.arr.length) return {done: true};
        let value = {
            value: this.arr[this.idx],
            done: false
        };
        this.idx++;
        return value;
    }
}

for (let value of Group.from(["a", "b", "c"])) {
    console.log(value);
}