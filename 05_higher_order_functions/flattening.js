let arrays = [[1, 2, 3], [4, 5], [6]];

function flatten(arrs) {
    return arrs.reduce((res, current) => res.concat(current), [])
}

console.log(flatten(arrays))