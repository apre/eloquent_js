function loop(init, condition, update, body) {
    let x = init
    while(condition(x)) {
        body(x)
        x = update(x)
    }
}

loop(3, n => n > 0, n => n - 1, console.log);
