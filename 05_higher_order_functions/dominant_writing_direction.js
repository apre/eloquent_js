function dominantDirection(text) {
    let direction = countBy(text, char => {
      let script = characterScript(char.codePointAt(0));
      return script ? script.direction : "none";
    }).filter(({direction}) => direction != "none")
      .reduce((bestDir, newDir) => bestDir.count > newDir.count ? bestDir: newDir, "")
      .name;

    return direction || 'ltr'
  }
  
  console.log(dominantDirection("Hello!"));
  // → ltr
  console.log(dominantDirection("Hey, مساء الخير"));
  // → rtl