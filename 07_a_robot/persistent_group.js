class PGroup {
    constructor(arr) {
        this.set = arr.reduce((set, next) => {
            if(!set.includes(next)) 
                set.push(next);
            return set;
        }, []);
        console.log(this.set);
    }

    add(item) {
        if(!this.has(item)) {
            return PGroup.from(this.set.concat(item))
        } else {
            return PGroup.from(this.set);
        }
    }

    delete(item) {
        return PGroup.from(this.set.filter(x => x !== item));
    }

    has(item) {
        return this.set.some(x => x === item);
    }

    static from(arr) {
        return new PGroup(arr)
    }

    static get empty() {
        return PGroup.from([])
    }
}

let a = PGroup.empty.add("a");
let ab = a.add("b");
let b = ab.delete("a");

console.log(b.has("b"));
// → true
console.log(a.has("b"));
// → false
console.log(b.has("a"));
// → false